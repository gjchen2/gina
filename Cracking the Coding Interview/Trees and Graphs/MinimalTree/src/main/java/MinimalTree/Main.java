package MinimalTree;

/**
 *
 * @author ginaj
 */
public class Main {

    public static class Node {
        private int value;
        private Node left;
        private Node right;
        
        public Node(int v) {
            this.value = v;
            this.left = null;
            this.right = null;
        }
    }
    
    public static void setNodeChildren(Node n, int start, int mid, int end, int[] arr) {
        if(start < end) {
            int leftIndex = (start + mid - 1) / 2;
            n.left = new Node(arr[leftIndex]);
            setNodeChildren(n.left, start, leftIndex, mid - 1, arr);

            int rightIndex = (mid + 1 + end) / 2;
            n.right = new Node(arr[rightIndex]);
            setNodeChildren(n.right, mid + 1, rightIndex, end, arr);
        }
    }
    
    public static Node minTree(int[] arr) {
        int mid = arr.length / 2;
        Node root = new Node(arr[mid]);
        setNodeChildren(root, 0, mid, arr.length, arr);
        return root;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
