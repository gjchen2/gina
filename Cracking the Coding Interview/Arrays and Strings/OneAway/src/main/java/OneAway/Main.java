package OneAway;
/**
 *
 * @author ginaj
 */
public class Main {

    public static boolean oneAway(String a, String b) {
        boolean sameLength = false;
        String one;
        String two;
        if(a.length() == b.length()) {
            sameLength = true;
            one = a;
            two = b;
        } else if(a.length() > b.length()) {
            one = b;
            two = a;
        } else {
            one = a;
            two = b;
        }
        
        int numDiff = 0;
        int i = 0;
        int j = 0;
        
        while(i < one.length() && j < two.length()) {
            if(one.charAt(i) == two.charAt(j)) {
                i++;
            } else {
                numDiff++;
                if(numDiff > 1) {
                    return false;
                }
                if(sameLength) {
                    i++;
                }
            }
            j++;
        }
        return true;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        String a = "pale";
        String b = "ple";

        System.out.println(String.format("%s, %s -> %b", a, b, oneAway(a, b)));
        
        String c = "pales";
        String d = "pale";
        
        System.out.println(String.format("%s, %s -> %b", c, d, oneAway(c, d)));
        
        String e = "pale";
        String f = "bale";
        
        System.out.println(String.format("%s, %s -> %b", e, f, oneAway(e, f)));
        
        String g = "pale";
        String h = "bake";
        
        System.out.println(String.format("%s, %s -> %b", g, h, oneAway(g, h)));
    }
    
}
