package StackOfPlates;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author ginaj
 */
public class Main {
    
    public static class SetOfStacks {
        private int stackCapacity;
        private int currStack;
        private List<Stack> stacks;
        
        public SetOfStacks(int stackCapacity) {
            this.stackCapacity = stackCapacity;
            this.currStack = 0;
            this.stacks = new ArrayList<>();
            Stack<Integer> stack = new Stack<>();
            this.stacks.add(stack);
        }
        
        public void push(int value) {
            Stack<Integer> curr = this.stacks.get(this.currStack);
            if(curr.size() < this.stackCapacity) {
                curr.push(value);
            } else {
                Stack<Integer> newStack = new Stack<>();
                newStack.push(value);
                this.stacks.add(newStack);
                this.currStack++;
            }
            System.out.println(String.format("Stack %d, size after pushing %d", this.currStack, curr.size()));
        }
        
        public int pop() {
            Stack<Integer> curr = this.stacks.get(this.currStack);
            if(curr.size() > 0) {
                System.out.println(String.format("Stack %d, size after popping %d", this.currStack, curr.size() - 1));
                return curr.pop();
            }
            if(this.currStack == 0) {
                System.out.println("No more in stacks!");
                return -1; //denotes error
            }
            this.stacks.remove(this.currStack);
            this.currStack--;
            curr = this.stacks.get(this.currStack);
            System.out.println(String.format("Stack %d, size after popping %d", this.currStack, curr.size() - 1));
            return curr.pop();
        }
        
        public int popAt(int index) {
            if(index >= this.stacks.size()) {
                System.out.println("Index out of bounds");
                return -1;
            }
            Stack<Integer> curr = this.stacks.get(index);
            return curr.pop();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SetOfStacks sos = new SetOfStacks(2);
        sos.push(1);
        sos.push(2);
        sos.push(3);
        sos.push(4);
        
        System.out.println(sos.popAt(0));
        
        System.out.println(sos.pop());
        System.out.println(sos.pop());
        System.out.println(sos.pop());
        System.out.println(sos.pop());
    }
    
}
