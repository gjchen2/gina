package AnimalShelter;

import java.util.LinkedList;

/**
 *
 * @author ginaj
 */
public class Main {

    abstract class Animal {
        private int order;
        private String name;
        
        public Animal(String name) {
            this.name = name;
        }
        
        public void setOrder(int order) {
            this.order = order;
        }
        
        public int getOrder() {
            return this.order;
        }
        
        public boolean isOlderThan(Animal a) {
            return this.order < a.getOrder();
        }
    }
    
    public class Dog extends Animal {
        public Dog(String name) {
            super(name);
        }
    }
    
    public class Cat extends Animal {
        public Cat(String name) {
            super(name);
        }
    }
    
    public static class AnimalShelter {
        
        private LinkedList<Dog> dogs;
        private LinkedList<Cat> cats;
        private int order;
        
        public AnimalShelter() {
            this.dogs = new LinkedList<>();
            this.cats = new LinkedList<>();
            this.order = 0;
        }
    
        public void enqueue(Animal a) {
            a.setOrder(this.order);
            this.order++;
            
            if(a instanceof Dog) {
                this.dogs.add((Dog) a);
            } else if(a instanceof Cat) {
                this.cats.add((Cat) a);
            }
        }
        
        public Animal dequeueAny() {
            if(this.dogs.isEmpty()) {
                return dequeueCat();
            }
            if(this.cats.isEmpty()) {
                return dequeueDog();
            }
            Dog dog = this.dogs.peek();
            Cat cat = this.cats.peek();
            if(dog.isOlderThan(cat)) {
                return dequeueDog();
            } else {
                return dequeueCat();
            }
        }
        
        public Animal dequeueCat() {
            return this.cats.poll();
        }
        
        public Animal dequeueDog() {
            return this.dogs.poll();
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
    }
    
}
